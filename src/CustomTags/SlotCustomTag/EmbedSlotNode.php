<?php


namespace Artw\Bitrix\Twig\CustomTags\SlotCustomTag;


use Twig\Compiler;
use Twig\Node\EmbedNode;
use Twig\Node\NodeOutputInterface;

class EmbedSlotNode extends EmbedNode implements NodeOutputInterface
{
    protected function addGetTemplate(Compiler $compiler)
    {
        $compiler->write('$this->loadTemplate(');
        $compiler->string($this->getAttribute('name'));
        $compiler->raw(', ');
        $compiler->repr($this->getTemplateName());
        $compiler->raw(', ');
        $compiler->repr($this->getTemplateLine());
        $compiler->raw(', ');
        $compiler->string($this->getAttribute('index'));
        $compiler->raw(')');
    }

    /**
     * Двойная логика: если есть атрибут value, это значит,
     * что в качестве пути к файлу передана обычная строка,
     * с которой мы можем заранее работать до компиляции.
     *
     * Если же атрибута нет, значит что в качестве пути использовалась какая-то переменная,
     * которую мы узнаем только на этапе компиляции. Поэтому всю логику обработки путей
     * нужно включать в скомпилированный PHP-файл. В первом случае в PHP-файл
     * включаем готовый результат, чтобы не тратить напрасно ресурсы.
     *
     * @param Compiler $compiler
     *
     * @see SlotTokenParser::setPreparedTemplatePath
     */
    protected function addTemplateArguments(Compiler $compiler)
    {
        $isOnly = false !== $this->getAttribute('only');

        $expr = $this->getNode("expr");

        if ($expr->hasAttribute("value")) {
            $defaultData2Php = SlotTokenParser::getDefaultData($expr->getAttribute("value"), false);
            $getDynamicData = false;
        } else {
            $defaultData2Php = false;
            $getDynamicData = true;
        }

        $addDefaultData = function () use ($expr, $compiler, $getDynamicData, $defaultData2Php) {
            if ($getDynamicData) {
                $compiler
                    ->raw(SlotTokenParser::class . "::getDefaultData(")
                    ->subcompile($expr)
                    ->raw(', true)');
            } else {
                $compiler->raw($defaultData2Php);
            }
        };

        if (!$this->hasNode('variables')) {
            if ($getDynamicData || $defaultData2Php) {
                $compiler->raw("twig_array_merge(");

                call_user_func($addDefaultData);

                $compiler->raw(", \$context)");
            } else {
                $compiler->raw(!$isOnly ? '$context' : '[]');
            }
        } elseif (!$isOnly) {
            /**
             * TODO: twig_array_merge мерджит только по две переменных.
             * Можно заменить на array_merge, но может потяряться какая-то сложная логика для объектов
             * Поэтому лучше пока оставить так
             *
             * @see https://tinyurl.com/y4cj6h3s
             */
            $compiler->raw("twig_array_merge(twig_array_merge(");

            call_user_func($addDefaultData);

            $compiler->raw(", \$context), ");
            $compiler->subcompile($this->getNode('variables'));
            $compiler->raw(')');
        } else {
            $compiler->raw("twig_array_merge(");

            call_user_func($addDefaultData);

            $compiler->raw(", ");
            $compiler->subcompile($this->getNode('variables'));
            $compiler->raw(')');
        }
    }
}
