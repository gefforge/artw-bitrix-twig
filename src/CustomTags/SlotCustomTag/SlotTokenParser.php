<?php


namespace Artw\Bitrix\Twig\CustomTags\SlotCustomTag;


use Artw\Bitrix\Twig\SingletonTrait;
use Artw\Bitrix\Twig\CustomTags\ViewCutomTag\ViewTokenParser;
use Twig\Node\Expression\ConstantExpression;
use Twig\Node\Expression\NameExpression;
use Twig\Token;
use Twig\TokenParser\EmbedTokenParser;
use Bitrix\Main\Application;
use Twig\Node\Node;

/**
 * @see EmbedTokenParser
 * Расширяет тег {% embed %}
 *
 * {% slot "template.twig" [with {some: 'values'} only] %}
 */
class SlotTokenParser extends EmbedTokenParser
{
    use SingletonTrait;

    private $pathParams;

    public function __construct()
    {
        /*
         * Дефолтные пути к views. Путь от корня сайта.
         *
         * Можно переопределить:
         * ViewTokenParser::getInstance()->setPathParams(["..."]);
         */
        $this->pathParams = ViewTokenParser::getInstance()->getPathParams();
    }

    public static function getPreparedPath($filePath, $ext = false)
    {
        return ViewTokenParser::getPreparedPath($filePath, $ext);
    }

    /**
     * @param      $templatePath
     * @param bool $isDynamicMode Если true, значит путь уже готовый, указывает на twig-файл,
     *                            а метод вызывается чтобы получить данные в строке
     *
     * @return mixed
     */
    public static function getDefaultData($templatePath, $isDynamicMode = false)
    {
        return ViewTokenParser::getDefaultData($templatePath, $isDynamicMode);
    }

    public function parse(Token $token)
    {
        $stream = $this->parser->getStream();

        $parent = $this->parser->getExpressionParser()->parseExpression();

        // TODO: Здесь логика преобразования пути к файлу
        self::getInstance()->setPreparedTemplatePath($parent);

        list($variables, $only, $ignoreMissing) = $this->parseArguments();

        $parentToken = $fakeParentToken = new Token(Token::STRING_TYPE, '__parent__', $token->getLine());
        if ($parent instanceof ConstantExpression) {
            $parentToken = new Token(Token::STRING_TYPE, $parent->getAttribute('value'), $token->getLine());
        } elseif ($parent instanceof NameExpression) {
            $parentToken = new Token(Token::NAME_TYPE, $parent->getAttribute('name'), $token->getLine());
        }

        // inject a fake parent to make the parent() function work
        $stream->injectTokens([
            new Token(Token::BLOCK_START_TYPE, '', $token->getLine()),
            new Token(Token::NAME_TYPE, 'extends', $token->getLine()),
            $parentToken,
            new Token(Token::BLOCK_END_TYPE, '', $token->getLine()),
        ]);

        $module = $this->parser->parse($stream, [$this, 'decideBlockEnd'], true);

        // override the parent with the correct one
        if ($fakeParentToken === $parentToken) {
            $module->setNode('parent', $parent);
        }

        $this->parser->embedTemplate($module);

        $stream->expect(Token::BLOCK_END_TYPE);

        return new EmbedSlotNode($module->getTemplateName(), $module->getAttribute('index'), $variables, $only, $ignoreMissing, $token->getLine(), $this->getTag());
    }

    public function decideBlockEnd(Token $token)
    {
        return $token->test('endslot');
    }

    public function getTag()
    {
        return 'slot';
    }

    /**
     * @return mixed
     */
    public function getPathParams()
    {
        return $this->pathParams;
    }

    /**
     * @param mixed $pathParams
     */
    public function setPathParams($pathParams)
    {
        $this->pathParams = $pathParams;
    }

    /**
     * Готовим путь к подключаемому файлу
     *
     * @param Node $expr
     */
    private function setPreparedTemplatePath($expr)
    {
        if ($expr->hasAttribute("value")) {
            $expr->setAttribute(
                "value",
                self::getPreparedPath($expr->getAttribute("value"))
            );
        }
    }
}
