<?

namespace Artw\Bitrix\Twig;


use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\IO\Directory;
use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;


/**
 * @property Twig_Environment engine
 * @property array            loaderPaths
 * @property array            namespacedPaths
 */
class TemplateEngine
{
    use SingletonTrait;

    public  $engine;
    private $cacheFolder     = "";
    private $loaderPaths     = [];
    private $namespacedPaths = [];
    private $renderParams    = [];

    /**
     * Инициализируются дефолтные параметры, которые могут быть в любой момент изменены через Singleton
     * примерно так:
     * Renderer::getInstance()->setRenderParams(...) // Переменные, которые будут переданы в шаблон
     * Renderer::getInstance()->setLoaderPaths(...)  // Пути, которых будут искаться подключаемые файлы
     */
    public function __construct()
    {
        $this->cacheFolder = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/cache/twig_cache";

        $this->setRenderParams(
            [
                "assetsRoot" => SITE_TEMPLATE_PATH . "/frontend/src/",
            ]
        );

        $this->setLoaderPaths(
            [
                Application::getDocumentRoot() . SITE_TEMPLATE_PATH . "/frontend/src/",
                Application::getDocumentRoot(),
            ]
        );
    }

    public static function bindEvents()
    {
        self::addCustomTemplateEngine();
    }

    public static function clearCache()
    {
        $dir = self::getInstance()->getCacheFolder();
        if (is_dir($dir)) {
            Directory::deleteDirectory($dir);
        }
    }

    public static function addCustomTemplateEngine()
    {
        global $arCustomTemplateEngines;

        $arCustomTemplateEngines["twig"] = [
            "templateExt" => ["twig"],
            "function"    => "artwRenderTwigTemplate",
        ];
    }

    /**
     * @return string
     */
    public function getCacheFolder()
    {
        return $this->cacheFolder;
    }

    /**
     * @return Twig_Environment
     */
    public function getEngine()
    {
        if (!is_object($this->twig)) {
            $this->initEngine();
        }

        return $this->twig;
    }

    /**
     * @return Twig_Environment
     */
    public function initEngine()
    {
        unset($this->twig);

        $loader = new Twig_Loader_Filesystem();

        foreach ($this->loaderPaths as $namespace => $path) {
            if (file_exists($path)) {
                if (is_numeric($namespace)) {
                    $loader->addPath($path);
                } else {
                    $loader->addPath($path, $namespace);
                }
            }
        }

        $this->twig = new Twig_Environment(
            $loader, [
                       "cache"      => static::isDevMode()
                           ? false
                           : $this->cacheFolder,
                       "debug"      => static::isDevMode(),
                       "autoescape" => false,
                   ]
        );

        $this->twig->addExtension(new Twig_Extension_Debug);
        $this->twig->addExtension(new Extension\RendererExtension);

        return $this->twig;
    }

    /**
     * @param array $loaderPaths
     */
    public function setLoaderPaths($loaderPaths)
    {
        $this->loaderPaths = array_unique(array_merge(
            (array)$this->loaderPaths,
            (array)$loaderPaths
        ));

        $this->initEngine();
    }

    /**
     * @param array $renderParams
     *
     * @return Renderer
     */
    public function setRenderParams(array $renderParams)
    {
        $this->renderParams = array_merge(
            (array)$this->renderParams,
            (array)$renderParams
        );

        return $this;
    }

    /**
     * @return array
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function getRenderParams()
    {
        return $this->renderParams;
    }

    /**
     * @return array
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function getLoaderPaths()
    {
        return $this->loaderPaths;
    }

    public static function isDevMode()
    {
        return $GLOBALS['USER']->IsAdmin() || isset($_REQUEST['clear_cache']);
    }

    /**
     * Рендерит произвольный twig-файл, возвращает результат в виде строки
     *
     * @param string $src     Путь к twig-файлу
     * @param array  $context Контекст
     *
     * @return string Результат рендера
     */
    public static function renderStandalone($src, $context = [])
    {
        return trim(self::getInstance()->getEngine()->render(
            $src,
            array_merge(
                (array)self::getInstance()->getRenderParams(),
                (array)$context
            )
        ));
    }

    public static function renderBitrixComponentTemplate(
        $templateFile,
        $arResult,
        $arParams,
        $arLangMessages,
        $templateFolder,
        $parentTemplateFolder,
        $template
    ) {
        global $APPLICATION;

        if (($f = Application::getDocumentRoot() . $templateFile) && file_exists($f)) {
            if ($_REQUEST['clear_cache'] == 'Y') {
                touch($f);
            }
        }

        // Битрикс не умеет 'лениво' грузить языковые сообщения если они запрашиваются из twig, т.к. ищет вызов
        // GetMessage, а после ищет рядом lang-папки. Т.к. рядом с кешем их конечно нет
        // Кроме того, Битрикс ждёт такое же имя файла, внутри lang-папки. Т.е. например template.twig
        // Но сам includ'ит их, что в случае twig файла конечно никак не сработает. Поэтому подменяем имя
        $templateMess = Loc::loadLanguageFile(
            $_SERVER['DOCUMENT_ROOT'] . preg_replace('/[.]twig$/', '.php', $template->GetFile())
        );

        // Это не обязательно делать если не используется lang, т.к. Битрикс загруженные фразы все равно запомнил
        // и они будут доступны через вызов getMessage в шаблоне. После удаления lang, можно удалить и этот код
        if (is_array($templateMess)) {
            $arLangMessages = array_merge($arLangMessages, $templateMess);
        }

        echo TemplateEngine::renderStandalone(
            $templateFile,
            array_merge(
                (array)compact(
                    'arParams',
                    'arResult',
                    'arLangMessages',
                    'template',
                    'templateFolder',
                    'parentTemplateFolder'
                ),
                (array)$arResult['TEMPLATE_DATA']
            )
        );

        $component_epilog = $templateFolder . '/component_epilog.php';
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $component_epilog)) {
            /** @var CBitrixComponent $component */
            $component = $template->getComponent();
            $component->SetTemplateEpilog(
                [
                    'epilogFile'     => $component_epilog,
                    'templateName'   => $template->__name,
                    'templateFile'   => $template->__file,
                    'templateFolder' => $template->__folder,
                    'templateData'   => false,
                ]
            );
        }
    }

    /**
     * Рендерит произвольный twig-файл, выводит результат в stdout
     *
     * @param string $src
     * @param array  $context
     */
    public static function displayStandalone($src, $context = [])
    {
        echo self::renderStandalone($src, $context);
    }

    /**
     * Рендерит произвольную twig-строку, возвращает результат в виде строки
     *
     * @param string $template Строка шаблона, использующего Twig-синтаксис
     * @param array  $params  Контекст
     *
     * @return string Результат рендера
     */
    public static function renderString($template, $params = [])
    {
        return self::getInstance()->getEngine()
                   ->createTemplate($template)
                   ->render(
                       array_merge(
                           (array)self::getInstance()->getRenderParams(),
                           (array)$params
                       )
                   );
    }

    /**
     * Рендерит произвольную twig-строку, выводит результат в stdout
     *
     * @param string $template Строка шаблона, использующего Twig-синтаксис
     * @param array  $context  Контекст
     */
    public static function displayString($template, $context = array())
    {
        echo static::renderString($template, $context);
    }
}
