<?php


namespace Artw\Bitrix\Twig;


trait SingletonTrait
{
    protected static $instance = null;

    /**
     * @return static|null
     * @noinspection PhpMissingReturnTypeInspection
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }
}
