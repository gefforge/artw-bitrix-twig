<?php

use Artw\Bitrix\Twig\TemplateEngine;

if (!function_exists('artwRenderTwigTemplate')) {
    /**
     * @param string                   $templateFile
     * @param array                    $arResult
     * @param array                    $arParams
     * @param array                    $arLangMessages
     * @param string                   $templateFolder
     * @param string                   $parentTemplateFolder
     * @param CBitrixComponentTemplate $template
     */
    function artwRenderTwigTemplate(
        $templateFile,
        $arResult,
        $arParams,
        $arLangMessages,
        $templateFolder,
        $parentTemplateFolder,
        $template
    ) {
        TemplateEngine::renderBitrixComponentTemplate(
            $templateFile,
            $arResult,
            $arParams,
            $arLangMessages,
            $templateFolder,
            $parentTemplateFolder,
            $template
        );
    }
}
